//
//  PPWebViewVC.m
//  Paving Planner
//
//  Created by Nik Lever on 21/05/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import "PPWebViewVC.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

@interface PPWebViewVC ()

@end

@implementation PPWebViewVC
@synthesize pp_wv;
@synthesize js;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index-ios" ofType:@"html" inDirectory:@"tablet"]];
    [pp_wv loadRequest:[NSURLRequest requestWithURL:url]];
    
    self.js = [self.pp_wv valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    // Provide an object for JS to access our exported methods by
    self.js[@"iosScriptObject"] = self;
    
}

-(void)jsAnalytics:(NSString *)str{
    str = [str stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"JavaScript: %@", str);
    
    NSRange pos = [str rangeOfString:@"#analytics"];
    
    if ( pos.location != NSNotFound){
        NSString *param = [str substringFromIndex:pos.location];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        NSString *str1;
        if (tracker!=nil){
            if ([param hasPrefix:@"#analytics://trackuser.com/"]){
                str1 = [param substringFromIndex:27];
                NSLog(@"Analytics: userId:%@", str);
                //[tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Paving Planner"  // Event category (required)
                //                                                      action:@"UserId"          // Event action (required)
                //                                                       label:str                // Event label
                //                                                       value:nil] build]];      // Event value
                [tracker set:@"&uid" value:str1];
            }else if ([param hasPrefix:@"#analytics://trackevent.com/"]){
                str1 = [[param substringFromIndex:28] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSArray *tokens = [str1 componentsSeparatedByString:@"|"];
                NSLog(@"Analytics: category:%@ action:%@ label:%@", [tokens objectAtIndex:0], [tokens objectAtIndex:1], [tokens objectAtIndex:2]);
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[tokens objectAtIndex:0]   // Event category (required)
                                                                        action:[tokens objectAtIndex:1]   // Event action (required)
                                                                        label:[tokens objectAtIndex:2]   // Event label
                                                                        value:nil] build]];              // Event value
            }
        }
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType; {
    NSString *url = [[request URL] absoluteString];
    
    //save form data
    if([url isEqualToString:@"about:blank"]) {
        NSString *currentPage = [self.pp_wv stringByEvaluatingJavaScriptFromString: @"currentPage"];
        
        if ([currentPage isEqualToString:@"login"]){
            //grab the data from the page
            NSString *email = [self.pp_wv stringByEvaluatingJavaScriptFromString: @"$('input#loginEmail').val()"];
            NSString *password = [self.pp_wv stringByEvaluatingJavaScriptFromString: @"$('input#loginPassword').val()"];
            //store values locally
            [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"email"];
            [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
        }
    }

    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    //verify view is on the login page of the site (simplified)
    NSString *currentPage = [self.pp_wv stringByEvaluatingJavaScriptFromString: @"currentPage"];

    if ([currentPage isEqualToString:@"login"]) {
        
        //check for stored login credentials
        NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
        NSString *password = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
        
        if (email.length != 0 ) {
            //create js strings
            NSString *loadEmailJS = [NSString stringWithFormat:@"$('input#loginEmail').val('%@')", email];
            NSString *loadPasswordJS = [NSString stringWithFormat:@"$('input#loginPassword').val('%@')", password];
            
            //autofill the form
            [self.pp_wv stringByEvaluatingJavaScriptFromString: loadEmailJS];
            [self.pp_wv stringByEvaluatingJavaScriptFromString: loadPasswordJS];
            
        }
    }   
}

@end
