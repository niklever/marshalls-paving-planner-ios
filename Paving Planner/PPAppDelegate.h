//
//  PPAppDelegate.h
//  Paving Planner
//
//  Created by Nik Lever on 21/05/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PPWebViewVC;

@interface PPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) PPWebViewVC *viewController;

@end
