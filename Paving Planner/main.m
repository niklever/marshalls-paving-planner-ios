//
//  main.m
//  Paving Planner
//
//  Created by Nik Lever on 21/05/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PPAppDelegate class]));
    }
}
