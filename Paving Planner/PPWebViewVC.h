//
//  PPWebViewVC.h
//  Paving Planner
//
//  Created by Nik Lever on 21/05/2014.
//  Copyright (c) 2014 Nik Lever. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JavaScriptCore/JavaScriptCore.h>

@protocol UserInfoJSExports <JSExport>
    -(void)jsAnalytics:(NSString*)msg;
@end

@interface PPWebViewVC : UIViewController<UserInfoJSExports, UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *pp_wv;
@property (nonatomic, readwrite, strong) JSContext *js;

@end
